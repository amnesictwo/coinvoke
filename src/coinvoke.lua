
function coinvoke ( ... )
   local args = {...}
   local new_t = {}
   local new_ret = {}
   
   if type(args) == 'table' then
      for i, v in ipairs(args) do
	 new_t[i] = spawn ( v )
      end
   end

   local flag_interrupter = false
   if type(new_t) == 'table' then
      for ks, is in ipairs(new_t) do
	 local status
	 if not flag_interrupter then
	    status = pcall(function () new_ret [ ks ] = is:join() end)
	 else
	    is:interrupt (  )
	 end
	 
	 if status == false then flag_interrupter = true end
      end
   end
   
   return unpack ( new_ret )
end

if _CONTEXT == "test" then
   
   -- for demonstration only
   local sleep_for = require('sleep_for')

   local function msk (n)
      return function ()
	 -- for debug/demonstration proposity
	 -- sleep_for ( n*1000 ) 
	 -- print ( "action :: ", n )
	 return n end
   end

   local function failmsk ( n )
      return function ()
	 sleep_for ( n*1000 )
	 -- print ( "action :: ", n )
	 error "proposiful error"
	 return n end
   end

   A = msk ( 2 )
   B = msk ( 5 )
   C = failmsk ( 6 )
   N = msk ( 10 )
   S = msk ( 15 )
   H = msk ( 1 )


   print ("begin test")
   
   local N_ret, A_ret, B_ret, S_ret, H_ret, C_ret = coinvoke ( N, A, B, C, S, H )

   assert(A_ret == 2,   'A Result Fail with ' .. tostring(A_ret))
   assert(B_ret == 5,   'B Result Fail with ' .. tostring(B_ret))
   assert(C_ret == nil, 'C Result Fail with ' .. tostring(C_ret))
   assert(H_ret == nil, 'H Result Fail with ' .. tostring(H_ret))
   assert(S_ret == nil, 'S Result Fail with ' .. tostring(S_ret))
   assert(N_ret == 10,  'N Result Fail with ' .. tostring(N_ret))
   
   print ("All Tests Pass")


   assert(2 + 2 == 4)
end
